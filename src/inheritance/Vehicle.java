package inheritance;

public class Vehicle {
	public String color;
	public int noOfWheels;
	public String model;
	
	Vehicle(String c,int n,String m){
		color = c;
		noOfWheels = n;
		model = m;
	}
	
	void display() {
		System.out.println("This is a "+this.getColor()+" exterior "+this.getModel()+" with "+ this.getNoOfWheels()+" wheels");
	}

	public String getColor() {
		return color;
	}

	public int getNoOfWheels() {
		return noOfWheels;
	}

	public String getModel() {
		return model;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public void setNoOfWheels(int noOfWheels) {
		this.noOfWheels = noOfWheels;
	}

	public void setModel(String model) {
		this.model = model;
	}
	

}
