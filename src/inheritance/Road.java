package inheritance;

public class Road {

	public static void main(String[] args) {
		Car c = new Car("Maroon",4,"Toyota Supra");
		Bus b = new Bus("Steel Blue",16,"Tata - Marcoplo");
		Truck t = new Truck("Charcoal",4,"Mercedes G63 AMG");
		
		c.display();
		b.display();
		t.display();
	}

}
