
import java.text.DecimalFormat;


public class Calculator {
	
	public void findAverage(double a,double b , double c) {
		
		double av = (a+b+c)/3;
		showResult(av);
		
	}
	
public void findAverage(double a,double b , double c,double d) {
		
		double av = (a+b+c+d)/4;
		showResult(av);
		
	}

public void findAverage(double a,double b , double c,double d,double e) {
	
	double av = (a+b+c+d+e)/5;
	showResult(av);
	
}

void showResult(double ans) {
	DecimalFormat df_obj = new DecimalFormat("#.##");
	System.out.println("The Average is : "+ df_obj.format(ans));
}
}
