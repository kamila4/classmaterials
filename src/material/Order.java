package material;
import java.util.Scanner;
public class Order {

	public static void main(String[] args) {
		Laptop l1 = new Laptop(5,3);
		Accessories a = new Accessories(10,5);
		
		System.out.println("Enter number of Laptops to order: ");
		Scanner sc = new Scanner(System.in);
		int nL = sc.nextInt();
		System.out.println("Enter number of Accessories to order: ");
		int nA = sc.nextInt();
		
		System.out.println("For Laptop: "+l1.order(nL) );
		System.out.println("For Accessories: "+a.order(nA) );
		// TODO Auto-generated method stub

	}

}
